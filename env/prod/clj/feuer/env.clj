(ns feuer.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[feuer started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[feuer has shut down successfully]=-"))
   :middleware identity})
