(ns feuer.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [feuer.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[feuer started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[feuer has shut down successfully]=-"))
   :middleware wrap-dev})
