CREATE TABLE VIIRS
(id INTEGER PRIMARY KEY,
 latitude TEXT,
 longitude  TEXT,
 bright_ti4 TEXT,
 scan TEXT,
 track TEXT,
 acq_date TIMESTAMP,
 acq_time VARCHAR(5),
 satellite CHAR,
 instrument VARCHAR(30),
 confidence CHAR,
 version VARCHAR(10),
 bright_ti5 TEXT,
 frp TEXT,
 daynight CHAR);
