FROM openjdk:8-alpine

COPY target/uberjar/feuer.jar /feuer/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/feuer/app.jar"]
